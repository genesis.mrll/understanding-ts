type Combinable = number | string;
type ConversionDescriptor = 'as-number' | 'as-text';

function isNumber(num: any): boolean {
  if (typeof num === 'number') {
    return true;
  } else {
    return false;
  }
}

function combine(
  input1: Combinable,
  input2: Combinable,
  resultConversion: ConversionDescriptor
) {
  let result;

  if (
    (isNumber(input1) && isNumber(input2)) ||
    resultConversion === 'as-number'
  ) {
    result = +input1 + +input2;
  } else {
    result = input1.toString() + input2.toString();
  }

  return result;
}

const result = combine(5, 2.8, 'as-number');
console.log(result);
