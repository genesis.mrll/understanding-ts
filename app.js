function isNumber(num) {
    if (typeof num === 'number') {
        return true;
    }
    else {
        return false;
    }
}
function combine(input1, input2, resultConversion) {
    var result;
    if ((isNumber(input1) && isNumber(input2)) ||
        resultConversion === 'as-number') {
        result = +input1 + +input2;
    }
    else {
        result = input1.toString() + input2.toString();
    }
    return result;
}
var result = combine(5, 2.8, 'as-number');
console.log(result);
