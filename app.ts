function add(num1: number, num2: number) {
  //Typescript infers that the return type is going to be a number
  return num1 + num2;
}

function printResult(num: number): void {
  console.log('Result: ' + num);
}

function printResult1(num: number): undefined {
  // Returning nothing returns the primative type undefined
  console.log('Result: ' + num);
  return;
}

let combineValues: (num1: number, num2: number) => number;
combineValues = add; //You can use funtions as types

printResult(add(1, 2));
